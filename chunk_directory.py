"""
Process a directory with (subdirectories and) audio files. For each file, generate chunks of some maximum size, and
write them to a destiny directory.
"""

import os
import os.path
import logging


import chunk_audio

audio_exts = set(['.mp3', '.wav'])


def process_tree(source_dir_, destiny_dir_, chunk_size_, audio_format_, prefix_):
    """
    Process a source directory with files. For each file inside, if it has a supported file extension, split it in two
    files and store them in the destiny directory. Save the files with the output extension.

    :param source_dir_: source directory with files to process.
    :param destiny_dir_: destiny directory in where to store processed files.
    :param chunk_size_: max size of output chunks, in miliseconds.
    :param audio_format_: format of exported audio, e.g. mp3.
    :param prefix_: prefix to use as the name of the exported files.
    :return:
    """
    for dir_path, dir_names, file_names in os.walk(source_dir_):
        for file_name in file_names:
            file_only_name, file_ext = os.path.splitext(file_name)
            if file_ext not in audio_exts:
                continue
            out_dir_path = dir_path.replace(source_dir_, destiny_dir_)
            if not os.path.isdir(out_dir_path):
                os.makedirs(out_dir_path)
            process_file_name = file_only_name
            in_file_path = os.path.join(dir_path, process_file_name + file_ext)
            logging.info('Processing file: ' + str(in_file_path))
            chunk_audio.process_audio_file(in_file_path, chunk_size_, out_dir_path, audio_format_, prefix_)


if __name__ == '__main__':
    import argparse

    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument('source_dir', help='the source directory with mp3 files.')
    parser.add_argument('destiny_dir', help='the destiny directory in where to output wav files.')
    parser.add_argument('--chunk-size', help='chunk size to split the files, in miliseconds.', type=int, default=10000)
    parser.add_argument('--audio-format', help='format to output the chunked files.', default='wav')
    parser.add_argument('--prefix', help='prefix to prepend to the output chunked files.')

    args = parser.parse_args()

    source_dir = args.source_dir
    destiny_dir = args.destiny_dir
    chunk_size = args.chunk_size
    audio_format = args.audio_format
    prefix = args.prefix

    process_tree(source_dir, destiny_dir, chunk_size, audio_format, prefix)