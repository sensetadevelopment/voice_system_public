"""
Separate an audio file in chunks of some maximum size in the silences.
"""
import os
import os.path

import pydub
import pydub.effects

coef_silence = 0.15


audio_exts = set(['.mp3', '.wav'])


def export_chunks_to_files(out_directory_, file_only_name, prefix_, counter, audio_format_, export_chunks):
    """
    Get a list of audio segments, combine them to one audio segment and export it as an audio file in the out directory.

    :param out_directory_: directory in where to put the generated audio files.
    :param file_only_name: name of original file without extension (without ".mp3" or ".wav").
    :param prefix_: prefix to use in the exported files, to use instead of the original file name.
    :param counter: a counter to use as part of the name of the exported file.
    :param audio_format_: format of audio, e.g. "mp3".
    :param export_chunks: list of audio segments with the chunks of the original file.
    :return:
    """
    if prefix_:
        use_name = prefix_
    else:
        use_name = file_only_name
    chunk_out_path = os.path.join(out_directory_, use_name + '_' + str(counter) + '.' + audio_format_)
    reduce(lambda x, y: x + y, export_chunks).export(chunk_out_path)


def process_audio_file(source_file_, chunk_size_, out_directory_, audio_format_, prefix_):
    """
    Process an audio file, get chunks of at maximum some length, separated by silences and save to a destiny directory.

    :param source_file_: source audio file path to process.
    :param chunk_size_: max size of output chunks, in miliseconds.
    :param out_directory_: directory in where to put generated files.
    :param audio_format_: format of exported audio, e.g. mp3.
    :param prefix_: prefix to use as the name of the exported files.
    :return:
    """
    path_name, file_ext = os.path.splitext(source_file_)
    assert file_ext.lower() in audio_exts

    original_directory, file_only_name = os.path.split(path_name)

    audio = pydub.AudioSegment.from_file(source_file_)
    norm_audio = pydub.effects.normalize(audio)
    silence_threshold = norm_audio.dBFS * (1 + coef_silence)
    audio_chunks = pydub.effects.split_on_silence(norm_audio, min_silence_len=500, silence_thresh=silence_threshold,
                                                  keep_silence=500)
    counter = 0
    export_chunks = []
    export_size = 0
    for chunk in audio_chunks:
        if len(chunk) == 0:
            continue
        if export_size + len(chunk) < chunk_size_:
            export_chunks.append(chunk)
            export_size += len(chunk)
        else:
            export_chunks_to_files(out_directory_, file_only_name, prefix_, counter, audio_format_, export_chunks)
            counter += 1
            export_chunks = [chunk]
            export_size = len(chunk)
    if len(export_chunks) > 0:
        export_chunks_to_files(out_directory_, file_only_name, prefix_, counter, audio_format_, export_chunks)


if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument('source_file', help='source file to split in chunks.')
    parser.add_argument('out_directory', help='directory to put the resulting files.')
    parser.add_argument('--chunk-size', help='chunk size to split the file, in miliseconds.', type=int, default=10000)
    parser.add_argument('--audio-format', help='format to output the chunked files.', default='wav')
    parser.add_argument('--prefix', help='prefix to prepend to the output chunked files.')
    args = parser.parse_args()

    source_file = args.source_file
    out_directory = args.out_directory
    chunk_size = args.chunk_size
    audio_format = args.audio_format
    prefix = args.prefix

    process_audio_file(source_file, chunk_size, out_directory, audio_format, prefix)
