"""
From one audio file, generate two files with left and right channels separated and write them to a destiny directory.
This program requires that ffmpeg from FFMPEG is installed, not ffmpeg from Libav.
"""

import subprocess

audio_exts = set(['.mp3', '.wav'])


def convert_audio_file(in_file_path, left_out_file_path, right_out_file_path):
    """
    Convert an audio file with two channels to two files, one for each channel.

    :param in_file_path: file path of original file with two channels.
    :param left_out_file_path: file path of output file for left channel.
    :param right_out_file_path: file path of output file for right channel.
    :return:
    """
    split_arg = ['ffmpeg',
                 '-i',
                 in_file_path,
                 '-map_channel',
                 '0.0.0',
                 left_out_file_path,
                 '-map_channel',
                 '0.0.1',
                 right_out_file_path]
    print ' '.join(split_arg)
    subprocess.call(split_arg)

if __name__ == '__main__':
    import argparse

    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument('source_file', help='the source audio file to separate channels.')
    parser.add_argument('destiny_left_file', help='the destiny output left channel file.')
    parser.add_argument('destiny_right_file', help='the destiny output right channel file.')
    args = parser.parse_args()

    source_file = args.source_file
    destiny_left_file = args.destiny_left_file
    destiny_right_file = args.destiny_right_file

    convert_audio_file(source_file, destiny_left_file, destiny_right_file)