Voice System
============

These scripts help separating audio files with two channels into separate files and splitting each audio file in chunks 
of some amount of miliseconds (by default 10), by the silences.

Technical Details
-----------------

The chunking works as follows for each audio file:

* The file is split in segments by the silences, which are detected by thresholding the audio, using a dynamic 
threshold generated based on the same audio file audio levels 
* The segments get a padding of original audio (in the beginning and in the ending) to avoid cutting them two early
* The segments are joined until they sum up to the desired amount of miliseconds and then get saved to a new audio file
* If a segment is bigger than the maximum amount of miliseconds, it is saved as is

The result is a directory that has multiple output audio files for each source file, each output file having at most 
some amount of miliseconds in length (in most cases) and without long silences (not even inside each audio file).
 
Because the segments are split by silences and then padded, it's possible that some poor quality audio files generate
output audio files with contiguous segments that got split, padded and then joined, generating a double padding that 
may contain repetitions of audio that was originally detected as silence, because of the original file's poor quality
(the noisy silence is not so distinguishable from the usable audio).

Requirements
------------

**You need to have installed FFmpeg: <https://www.ffmpeg.org/>**
And it should be accessible in the PATH, you should be able to type in the command line:

```
ffmpeg -version
```

and get something like:

```
ffmpeg version 0.10.12-7:0.10.12-1frodo0~precise
built on Feb 21 2015 14:17:37 with gcc 4.6.3
configuration: --arch=amd64 --disable-stripping --enable-pthreads --enable-runtime-cpudetect 
--extra-version='7:0.10.12-1frodo0~precise' --libdir=/usr/lib/x86_64-linux-gnu --prefix=/usr 
--enable-bzlib --enable-libdc1394 --enable-libfreetype --enable-frei0r --enable-gnutls --enable-libgsm 
--enable-libmp3lame --enable-librtmp --enable-libopencv --enable-libopenjpeg --enable-libpulse 
--enable-libschroedinger --enable-libspeex --enable-libtheora --enable-vaapi --enable-vdpau 
--enable-libvorbis --enable-libvpx --enable-zlib --enable-gpl --enable-postproc --enable-libcdio --enable-x11grab 
--enable-libx264 --shlibdir=/usr/lib/x86_64-linux-gnu --enable-shared --disable-static
libavutil      51. 35.100 / 51. 35.100
libavcodec     53. 61.100 / 53. 61.100
libavformat    53. 32.100 / 53. 32.100
libavdevice    53.  4.100 / 53.  4.100
libavfilter     2. 61.100 /  2. 61.100
libswscale      2.  1.100 /  2.  1.100
libswresample   0.  6.100 /  0.  6.100
libpostproc    52.  0.100 / 52.  0.100
```

**Note**: Ubuntu and other linux distributions (in some versions) have Libav in their package repositories, it's a 
fork of FFmpeg that breaks some APIs, but also includes a program named "ffmpeg", so it's possible to have a "ffmpeg" 
program that is not the needed one. Up to Ubuntu 14.10, to install FFmpeg, you need to add a PPA and install it from 
there. The FFmpeg projet has a PPA recommended for the latest Ubuntu version: <https://www.ffmpeg.org/download.html>.

For testing in Ubuntu 12.04, this PPA was used: <https://launchpad.net/~frodo-vdr/+archive/ubuntu/main>

**To add a PPA**:

* Go to the PPA website, e.g. "<https://launchpad.net/~mc3man/+archive/ubuntu/trusty-media>"
* Find the ID of the PPA, e.g. "ppa:mc3man/trusty-media"
* Add that PPA with:
```
sudo add-apt-repository ppa:mc3man/trusty-media 
```
* Then you can install ffmpeg with:
```
sudo apt-get install ffmpeg
```

**You need to have installed the `pydub` Python library.**
 
You can install it with:

```
pip install pydub
```

Usage
-----

**Separating channels of one file**

To separate the two channels of one audio file run:
 
```
python separate_audio_channels.py source_file destiny_left_file destiny_right_file
```
 
For example:
 
```
python separate_audio_channels.py /home/user/test_audio.wav /home/user/test_audio_l.mp3 /home/user/test_audio_r.mp3
```
 
You can get more information with:
 
```
python separate_audio_channels.py -h
```

**Separating channels of all the audio files in a directory**

To separate all audio files in a directory having 2 audio channels, with different information in each channel, and 
output the results to different audio files in another directory, run:
 
```
python separate_directory.py source_dir destiny_dir --output-ext .ext
```
 
For example:
 
```
python separate_directory.py /home/user/audio_in/ /home/user/audio_out/ --output-ext .mp3 
```
 
Or:
 
```
python separate_directory.py /home/user/audio_in/ /home/user/audio_out/ 
```
 
You can get more information with:
 
```
python separate_directory.py -h
```
 
**Chunking audio segments of one file**

To split one audio file in chunks by the silences run:
 
```
python chunk_audio.py source_file out_directory --chunk-size --audio-format --prefix
```

The optional parameters have these defaults:

* `chunk-size`: 10000 (miliseconds)
* `audio-format`: wav
* `prefix`: No prefix, by default the original file name is used
 
For example:
 
```
python chunk_audio.py /home/user/test_audio.wav /home/user/out/ --chunk-size 20000 --audio-format mp3 --prefix yyy
```
 
You can get more information with:
 
```
python chunk_audio.py -h
```

**Chunking audio segments of all audio files in a directory**

To split all audio files in a directory in chunks by the silences run:
 
```
python chunk_directory.py source_dir out_directory --chunk-size --audio-format --prefix
```

The optional parameters have these defaults:

* `chunk-size`: 10000 (miliseconds)
* `audio-format`: wav
* `prefix`: No prefix, by default the original file name is used, if you use a prefix, files will be overwritten
 
For example:
 
```
python chunk_directory.py /home/user/audio_in/ /home/user/out/ --chunk-size 20000 --audio-format mp3
```
 
You can get more information with:
 
```
python chunk_directory.py -h
```